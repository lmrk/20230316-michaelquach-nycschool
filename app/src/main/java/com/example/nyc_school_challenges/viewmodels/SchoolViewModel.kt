package com.example.nyc_school_challenges.viewmodels

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.nyc_school_challenges.domain.Result
import com.example.nyc_school_challenges.domain.SchoolModel
import com.example.nyc_school_challenges.domain.State
import com.example.nyc_school_challenges.repositories.SchoolsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import javax.inject.Inject

@HiltViewModel
class SchoolViewModel @Inject constructor(private val repository: SchoolsRepository) : ViewModel() {

    private val stateFlow = MutableStateFlow<State>(State.Ready())
    val state: StateFlow<State>
        get() = stateFlow

    private val searchResultFlow = MutableStateFlow<List<SchoolModel>>(listOf())
    val searchResult: StateFlow<List<SchoolModel>>
        get() = searchResultFlow

    private val currentSelectionFlow = MutableStateFlow<SchoolModel>(SchoolModel())
    val currentSelection: StateFlow<SchoolModel>
        get() = currentSelectionFlow

    @OptIn(DelicateCoroutinesApi::class)
    fun fetchSchools() {
        if (stateFlow.value.message != Result.READY) {
            return
        }

        stateFlow.value = State.Loading()

        viewModelScope.launch {
            repository.getSchoolModel(
                object : SchoolsRepository.SchoolsCallback {
                    override fun onSuccess(schools: List<SchoolModel>) {
                        stateFlow.value = State.Success(schools.sortedBy { it.schoolName })
                        search("")
                    }

                    override fun onFailure(e: Throwable) {
                        stateFlow.value = State.Failure(e)
                    }
                }
            )
        }
    }

    fun select(schoolModel: SchoolModel) {
        currentSelectionFlow.value = schoolModel
    }

    fun search(query: String) {
        val schools = (stateFlow.value as? State.Success)?.schools
        if (schools != null) {
            searchResultFlow.value = schools.filter {
                it.schoolName.contains(query, true)
            }
        }
    }

    private val messageFlow = MutableStateFlow("")
    val message: StateFlow<String>
        get() = messageFlow

    fun loadMessage() {
        viewModelScope.launch {
            messageFlow.value = "Greetings!"
        }
    }

    init {
        fetchSchools()
        search("")
    }
}
