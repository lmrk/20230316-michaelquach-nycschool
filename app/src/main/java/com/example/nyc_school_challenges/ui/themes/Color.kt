package com.example.nyc_school_challenges.ui.themes

import androidx.compose.ui.graphics.Color

val Green200 = Color(0xFF98FC86)
val Green500 = Color(0xFF2CEE00)
val Green700 = Color(0xFF0C6F01)
val Teal200 = Color(0xFF03DAC5)
