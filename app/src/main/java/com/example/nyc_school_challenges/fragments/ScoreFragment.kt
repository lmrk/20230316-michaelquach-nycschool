package com.example.nyc_school_challenges.fragments

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.ViewModelProvider
import com.example.nyc_school_challenges.databinding.FragmentScoreBinding
import com.example.nyc_school_challenges.viewmodels.SchoolViewModel

class ScoreFragment : DialogFragment() {

    private lateinit var binding: FragmentScoreBinding
    private lateinit var viewModel: SchoolViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            STYLE_NORMAL,
            android.R.style.Theme_Material_Light_NoActionBar_Fullscreen
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = FragmentScoreBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel = ViewModelProvider(requireActivity())[SchoolViewModel::class.java]
        val currentSelection = viewModel.currentSelection.value

        binding.apply {
            schoolName.text = currentSelection.schoolName
            schoolAddress.text = currentSelection.addressString
            phone.text = currentSelection.phone
            website.text = currentSelection.website
            email.text = currentSelection.email

            mathScore.text = currentSelection.satScores?.satMathAvgScore ?: "N/A"
            criticalReadingScore.text = currentSelection.satScores?.satCriticalReadingAvgScore ?: "N/A"
            writingScore.text = currentSelection.satScores?.satWritingAvgScore ?: "N/A"
            numTaskTaker.text = currentSelection.satScores?.numOfSatTestTakers ?: "N/A"

            schoolAddressLayout.setOnClickListener {
                // Opens map
                val lat = currentSelection.school?.latitude ?: "0.0"
                val lng = currentSelection.school?.longitude ?: "0.0"
                val label = currentSelection.schoolName
                val uri = "geo:$lat,$lng?q=$lat,$lng($label)"
                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(uri))

                startActivity(intent)
            }

            emailLayout.setOnClickListener {
                val email = currentSelection.emailUrl
                val intent = Intent(Intent.ACTION_SENDTO)

                intent.data = email
                startActivity(intent)
            }
            phoneLayout.setOnClickListener {
                val phone = currentSelection.phoneURL
                val intent = Intent(Intent.ACTION_DIAL)

                intent.data = phone
                startActivity(intent)
            }
            websiteLayout.setOnClickListener {
                val website = currentSelection.webSiteUrl
                val intent = Intent(Intent.ACTION_VIEW)

                intent.data = website
                startActivity(intent)
            }
            close.setOnClickListener {
                dismiss()
            }
        }
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ScoreFragment()
    }
}
