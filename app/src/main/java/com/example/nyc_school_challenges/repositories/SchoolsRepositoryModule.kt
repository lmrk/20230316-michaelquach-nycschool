package com.example.nyc_school_challenges.repositories

import com.example.nyc_school_challenges.network.NycApiDelegate
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
object SchoolsRepositoryModule {
    @Provides
    fun providesSchoolsRepository(api: NycApiDelegate): SchoolsRepository {
        return SchoolsRepository(api)
    }
}
