package com.example.nyc_school_challenges.network

import com.example.nyc_school_challenges.domain.HighSchool
import com.example.nyc_school_challenges.domain.SATScore
import retrofit2.http.GET

interface NycApiDelegate {
    @GET("s3k6-pzi2.json")
    suspend fun highSchools(): List<HighSchool>

    @GET("f9bf-2cp4.json")
    suspend fun satScores(): List<SATScore>
}
