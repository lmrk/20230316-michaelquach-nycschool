package com.example.nyc_school_challenges

import com.example.nyc_school_challenges.network.NycApiNetworkModule
import com.example.nyc_school_challenges.repositories.SchoolsRepositoryModule
import com.example.nyc_school_challenges.viewmodels.SchoolViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.*
import org.junit.Assert.assertEquals
import org.junit.Test

class ViewModelTest {

    private lateinit var viewModel: SchoolViewModel

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testViewModel() = runTest {

        val testDispatcher = UnconfinedTestDispatcher(testScheduler)
        Dispatchers.setMain(testDispatcher)

        try {
            viewModel = SchoolViewModel(
                SchoolsRepositoryModule.providesSchoolsRepository(
                    NycApiNetworkModule.providesNYCOpenDataAPI(
                        NycApiNetworkModule.providesRetrofit()
                    )
                )
            )

            viewModel.fetchSchools()
            print(viewModel.state.value)
            assertEquals(
                com.example.nyc_school_challenges.domain.Result.SUCCESS,
                viewModel.state.value.message
            )

        } finally {
            Dispatchers.resetMain()
        }

    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun settingMainDispatcher() = runTest {
        val testDispatcher = UnconfinedTestDispatcher(testScheduler)
        Dispatchers.setMain(testDispatcher)

        try {
            viewModel = SchoolViewModel(
                SchoolsRepositoryModule.providesSchoolsRepository(
                    NycApiNetworkModule.providesNYCOpenDataAPI(
                        NycApiNetworkModule.providesRetrofit()
                    )
                )
            )
            viewModel.loadMessage() // Uses testDispatcher, runs its coroutine eagerly
            assertEquals("Greetings!", viewModel.message.value)
        } finally {
            Dispatchers.resetMain()
        }
    }
}