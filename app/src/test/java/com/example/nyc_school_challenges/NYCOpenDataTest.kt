package com.example.nyc_school_challenges

import com.example.nyc_school_challenges.domain.SchoolModel
import com.example.nyc_school_challenges.network.NycApiNetworkModule
import com.example.nyc_school_challenges.repositories.SchoolsRepository
import com.example.nyc_school_challenges.repositories.SchoolsRepositoryModule
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test

class NYCOpenDataTest {
    //test repository
    private lateinit var repository: SchoolsRepository

    @Before
    fun setUp() {
        repository = SchoolsRepositoryModule.providesSchoolsRepository(
            NycApiNetworkModule.providesNYCOpenDataAPI(
                NycApiNetworkModule.providesRetrofit()
            )
        )
    }

    @Test
    fun testRepository() {
        runBlocking {
            repository.getSchoolModel(object : SchoolsRepository.SchoolsCallback {

                override fun onSuccess(schools: List<SchoolModel>) {
                    assert(true)
                }

                override fun onFailure(e: Throwable) {
                    assert(false)
                }
            })
        }
    }
}